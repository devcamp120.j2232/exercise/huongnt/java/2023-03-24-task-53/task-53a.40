public class InvoiceItem {
        // khai báo thuộc tính
        public int id;
        public String desc;
        public int qty;
        public double unitPrice;
         // phương thức khởi tạo
        public InvoiceItem(int id, String desc, int qty, double unitPrice) {
            this.id = id;
            this.desc = desc;
            this.qty = qty;
            this.unitPrice = unitPrice;
        }
                //getter
        public int getId() {
            return id;
        }
        public String getDesc() {
            return desc;
        }
        public int getQty() {
            return qty;
        }
        public double getUnitPrice() {
            return unitPrice;
        }
        

        //setter
        public void setQty(int qty) {
            this.qty = qty;
        }
        public void setUnitPrice(double unitPrice) {
            this.unitPrice = unitPrice;
        }

        //phương thức khác
        public double getTotal(){
            return unitPrice*qty;

        }

         
        //in ra console log
        @Override
        public String toString(){
            return String.format("Invoice [id = %s, desc = %s, qty = %s, unitprice = %s]", id, desc, qty, unitPrice);
        }
        
}
