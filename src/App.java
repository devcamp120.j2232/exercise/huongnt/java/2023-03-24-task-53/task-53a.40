public class App {
    public static void main(String[] args) throws Exception {
              // khởi tạo đối tượng invoice có tham số
              InvoiceItem invoice1 = new InvoiceItem(1, "Shoes", 2, 120000.00d);
              InvoiceItem invoice2 = new InvoiceItem(2, "Skirts", 4, 100000.00d);
      
              System.out.println("Invoice 1: Total");
              System.out.println(invoice1.getTotal());

              System.out.println("Invoice 2: Total");
              System.out.println(invoice2.getTotal());
    }
}
